;; Useful macros
(defun repeat (n list)
  (loop for i below n
     collect list))

(defmacro nnth (x y list)
  `(nth ,y (nth ,x ,list)))

(defmacro take (n list)
  `(subseq ,list 0 ,n))

(defmacro drop (n list)
  `(subseq ,list ,n))

(defun n-randoms (n &optional (limit 2) (inc 0))
  (loop for i from 1 to n
       collect (+ (random limit) inc)))



;;; PACKAGE DEFINITIONS
(ql:quickload '(:cl-markup :cl-json :postmodern :cl-mop :hunchentoot :optima))

;; Example for why postmodern is so fucking cool
;; Instantly maps this class to a USERS table in a database. No need for
;; bullshit conversion boilerplate on the way into or out of the database.
;; Bit like an ORM, but more low level control over when the objects and
;; databases get sync'd
(defclass users ()
  ((id :reader id
       :initarg :id
       :initform 0
       :col-type serial)
   (username :initarg :username
             :accessor username
             :col-type (varchar 25))
   (pass :initarg :pass
         :accessor pass
         :col-type (varchar 40))
   (email :initarg :email
          :accessor email
          :col-type (varchar 40))
   (role :initarg :role
         :accessor role
         :col-type integer))
  (:metaclass dao-class)
  (:keys id))

(deftable users
  (!dao-def))



;; Metaobject Protocol lets me take instances of these definitions and parse
;; them directly to JSON too. Jesus christ.

(defclass tag-post ()
  ((post-id :initarg :post
            :accessor post
            :col-type integer)
   (tag-name :initarg :tag
             :accessor tag
             :col-type (varchar 50)))
  (:metaclass dao-class)
  (:keys post-id tag-name))

(deftable tag-post
  (!dao-def)
  (!foreign 'post 'post-id 'id :on-delete :cascade :on-update :cascade)
  (!foreign 'tag 'tag-name 'name :on-delete :cascade :on-update :cascade))

(defvar a (dao-table-definition 'post))

;; Not sure how serializable columns should be handled yet.
;; Not a big fan of this approach, but don't have better ideas yet either.
(defun next-id (tablename)
  (1+
   (caar (query (:select (:max :id) :from tablename)))))

(insert-dao user)
(update-dao user)
(select-dao user)
(delete-dao user)
  
;; Fuckin neato MOP conversions.
;; Prevents huge amount of overlap in json conversions
;; Consider: Metaclass to support this in the future? Already discovered it
;; would have to be a seperate class from a DAO metaclass, meaning 2x number of
;; model definitions.
;; Would be a good opportunity to hook into a GraphQL metaclass though
(ql:quickload '(cl-mop cl-json))
(use-package :c2mop)
(use-package :cl-json)

(defun assoc-zip (keys values)
  (mapcar #'cons keys values))

(defun slot-names (class-name)
  (let ((class (find-class class-name)))
    (c2mop:ensure-finalized class)
    (mapcar #'c2mop:slot-definition-name (c2mop:class-slots class))))

(defun slot-values (class-instance)
  (mapcar #'(lambda (x) (handler-case (slot-value class-instance x)
                          (unbound-slot () nil)))
          (class-names (type-of class-instance))))

(defun assoc-item (class-instance)
  (assoc-zip (slot-names (type-of class-instance))
             (slot-values class-instance)))

(defun jsonify (class-instance &key (string t))
  (if string
      (cl-json:encode-json-to-string (assoc-item class-instance))
      (cl-json:encode-json (assoc-item class-instance))))

(defun jsonify-list (class-instances &key (string t))
  (let*  ((elements (mapcar #'assoc-item class-instances))
          (indexes (loop for i below (length class-instances) collect i))
          (zipped (assoc-zip indexes elements)))
    (if string
        (cl-json:encode-json-to-string zipped)
        (cl-json:encode-json zipped))))

